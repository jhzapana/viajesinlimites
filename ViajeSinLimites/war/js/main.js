$(document).ready(main);
 
var contador = 1;
var $nav = $('#nav1');

var $navTop = $nav.offset().top;


var pegarNav = function(){
	var $scrollTop = $(window).scrollTop();
	if($scrollTop >= $navTop ){
		$nav.addClass('sticky')
		
	}else{
		$nav.removeClass('sticky')
	}
}

$('#creardiv').siblings().hide();
$('#creardiv').hide();

$('#barra_paquete').siblings().hide();
$('#barra_paquete').hide();

$('#crear_servicios').hide();

$('#respuesta_admin').show();



function main () {
	
	

	
	$('.menu_bar').click(function(){
		if (contador == 1) {
			$('nav').animate({
				left: '0'
			});
			contador = 0;
		} else {
			contador = 1;
			$('nav').animate({
				left: '-100%'
			});
		}
	});
 
	// Mostramos y ocultamos submenus
	$('.submenu').click(function(){
		$(this).children('.children').slideToggle();
	});

	$('#log').click(function(){
		$('#modal').addClass('abrir_modal');
		
		
	});
	$('#cerrar').click(function(){
		$('#modal').addClass('cerrar_modal');
	});

	
	$(window).on('scroll',pegarNav);
	
	
	
	$('#form_registro').submit(function(){
		
		$.ajax({
			url: 'registro',
			type: 'POST',
			dataType: 'json',
			data: $('#form_registro').serialize(),
			success: function(data){
				if(data.isValid){
					alert('Su nombre es: '+data.nombre);
					setTimeout ("window.location.href = 'inicio';", 1000); 		
				}
				else{
					alert('Datos Incorrectos');
				}
					
			
			}
			
		});
		return false;
		
	});
	$('#btn_modificar').on("click",function(){
		$('#modificardiv').siblings().hide();
		$('#modificardiv').show();
		$('#btn_modificar').addClass('btn_active');
		$('#btn_modificar').siblings().removeClass('btn_active');
		
	});
	
	$('#mispaquetes').on("click",function(){
		
		$('#div_mispaquetes').show();
		$('#div_mispaquetes').siblings().hide();
		$('#mispaquetes').siblings().removeClass('btn_active');
		$('#mispaquetes').addClass('btn_active');
		
		
	});
	
	$('#btn_gestionar_paquete').on("click",function(){
		$('#barra_paquete').siblings().hide();
		$('#crearPaquete').siblings().hide();
		$('#barra_paquete').show();
		$('#btn_gestionar_paquete').siblings().removeClass('btn_active');
		$('#btn_gestionar_paquete').addClass('btn_active');
		$('#respuesta_admin').show();
		
	});
	$('#btn_crear_paquete').on("click",function(){
		$('#crearPaquete').siblings().hide();
		$('#crearPaquete').show();
		$('#btn_crear_paquete').addClass('btn_active');
		$('#respuesta_admin').show();
	});
	
	
	$('#btn_gestionar_administrador').on("click",function(){
		$('#barra_admin').siblings().hide();
		$('#barra_admin').show();
		$('#btn_gestionar_administrador').siblings().removeClass('btn_active');
		$('#btn_gestionar_administrador').addClass('btn_active');
	});
	
	$('#btn_crear_admin').on("click",function(){
		$('#btn_crear_admin').siblings().removeClass('btn_active');
		$('#btn_crear_admin').addClass('btn_active');
	});
	
	
	$('#btn_gestionar_servicios').on("click",function(){
		$('#barra_servicios').siblings().hide();
		$('#crear_servicios').siblings().hide();
		$('#barra_servicios').show();
		$('#btn_gestionar_servicios').siblings().removeClass('btn_active');
		$('#btn_gestionar_servicios').addClass('btn_active');
		$('#respuesta_admin').show();
		
	});
	$('#btn_crear_servicios').on("click",function(){
		$('#btn_crear_admin').siblings().removeClass('btn_active');
		$('#btn_crear_admin').addClass('btn_active');
		$('#crear_servicios').siblings().hide();
		$('#crear_servicios').show();
		$('#respuesta_admin').show();
	});
	
	
	$('#btn_gestionar_proveedor').on("click",function(){
		$('#barra_proveedores').siblings().hide();
		$('#crear_proveedores').siblings().hide();
		$('#barra_proveedores').show();
		$('#btn_gestionar_proveedor').siblings().removeClass('btn_active');
		$('#btn_gestionar_proveedor').addClass('btn_active');
		$('#respuesta_admin').show();
		
	});
	
	$('#btn_crear_proveedores').on("click",function(){
		$('#btn_crear_proveedores').siblings().removeClass('btn_active');
		$('#btn_crear_proveedores').addClass('btn_active');
		$('#crear_proveedores').siblings().hide();
		$('#crear_proveedores').show();
		$('#respuesta_admin').show();
	});
	
	
	
	$('#btn_modificar_proveedores').on("click",function(){
		$('#btn_modificar_proveedores').siblings().removeClass('btn_active');
		$('#btn_modificar_proveedores').addClass('btn_active');
		$('#modificar_proveedor').siblings().hide();
		$('#modificar_proveedor').show();
		$('#respuesta_admin').show();
	});
	
	
	
	
	$('#form_modificar').submit(function(){
		
		$.ajax({
			url: 'modificarCliente',
			type: 'POST',
			dataType: 'json',
			data: $('#form_modificar').serialize(),
			success: function(data){
				if(data.isValid){
					$('#repuestaCliente').html('Los Datos han  sido Modificados:'+data.username)
						
				}
				else{
					alert('Datos Incorrectos');
				}
					
			
			}
			
		});
		return false;
		
	});
	
	
	$('#form_login').submit(function(){
		
		$.ajax({
			url: 'login',
			type: 'POST',
			dataType: 'json',
			data: $('#form_login').serialize(),
			success: function(data){
				if(data.error){
				
					
					$('#respuesta_login').html('<h2><span class="iconM-cross"></span>Los Datos son Incorrectos:</h2> ');
				}
				if(data.client){
				
					
					setTimeout ("window.location.href = 'inicio.jsp';", 100); 
				}
				if(data.admin){
					setTimeout ("window.location.href = 'inicio_admin.jsp';", 100); 
				}
			}
		});
		return false;
		
	});
	$('#btn_gestionar_paquete').click(function(event) {
		
		$.ajax({
			url: 'gestionPaquete',
			type: 'GET',
			dataType: 'json',
			success: function(data){
				if(data.status){
				
					
					$('#respuesta_admin').html('Se encontraron: '+data.numero+' paquetes');
				}
				
			}
		});
		return false;
	});
	
	$('#mispaquetes').click(function(event) {
		
		$.ajax({
			url: 'consultarPaquete',
			type: 'GET',
			dataType: 'json',
			success: function(data){
				if(data.exito){
				
					$('#div_mispaquetes').load('misPaquetes.jsp');
					
				}
				
				
			}
		});
		return false;
	});
	
	
	$('#form_crearPaquete').submit(function(){
		
		$.ajax({
			url: 'crearPaquete',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crearPaquete').serialize(),
			success: function(data){
				if(data.exito){
					$('#respuesta_admin').html('Paquete creado:' + data.paquete);
				}
				
			}
		});
		return false;
		
	});
	
	$('#form_crear_rubro').submit(function(){
		
		$.ajax({
			url: 'crearRubro',
			type: 'GET',
			dataType: 'json',
			data: $('#form_crear_rubro').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('<h3>Rubro creado:' + data.rubro+'</h3>');
				}
				
			}
		});
		return false;
		
	});
	
	$('#form_crear_proveedor').submit(function(){
		
		$.ajax({
			url: 'crearProveedor',
			type: 'POST',
			dataType: 'json',
			data: $('#form_crear_proveedor').serialize(),
			success: function(data){
				if(data.isValid){
					$('#respuesta_admin').html('<h3>Proveedor  creado exitosamente :' + data.proveedor+'</h3>');
				}
				
			}
		});
		return false;
		
	});
	
	
	
	
	 $('#selectC').change(function(event) {  
	        var $country=$("select#selectC").val();
	           $.get('llenarRubro',{countryname:$country},function(responseJson) {   
	        	   var $select = $('#select_rubros');                           
	               $select.find('option').remove();                          
	               $.each(responseJson, function(key, value) {               
	                   $('<option>').val(key).text(value).appendTo($select);      
	                    });
	            });
	        });
	 
	 $('#selectModProv').change(function(event) {  
	        var $country=$("select#selectModProv").val();
	           $.get('llenarProv',{countryname:$country},function(responseJson) {   
	        	   var $select = $('#select_proveedores');                           
	               $select.find('option').remove();                          
	               $.each(responseJson, function(key, value) {               
	                   $('<option>').val(key).text(value).appendTo($select);      
	                    });
	            });
	        });
	 
		$('#modificarEsteProv').submit(function(){
			
			$.ajax({
				url: 'seleccionarProveedor',
				type: 'POST',
				dataType: 'json',
				data: $('#modificarEsteProv').serialize(),
				success: function(data){
					if(data.exito){
						
						$('#buscando_prov').load('miProveedor.jsp');
						
					}
					
				}
			});
			return false;
			
		});
	
	
};

