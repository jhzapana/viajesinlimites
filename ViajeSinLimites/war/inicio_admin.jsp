<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import = "model.*" %>
<%@ page import="java.util.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page import="java.text.SimpleDateFormat"%>

<% HttpSession misesion= request.getSession(); %>
<%
//if( (getServletContext().getAttribute("votantes")) != null ){
	String tipo = (String)getServletContext().getAttribute("tipo");
	if(tipo != null && !tipo.equals("superusuario")) {
		Administrador admin = (Administrador) getServletContext().getAttribute("admin");
	}
	
//}
%>
<%
  	 	Date dNow = new Date();
   		SimpleDateFormat ft = 
  	 	new SimpleDateFormat ("dd/MM/yyyy");
  	 	String currentDate = ft.format(dNow);
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Inicio Administrador</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link rel="stylesheet" href="css/ed-grid.css">
	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/estilo_admin.css">
	<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	

	<SCRIPT>
    function crearCampos(cantidad){
        var div = document.getElementById("campos_dinamicos");
        while(div.firstChild)div.removeChild(div.firstChild); // remover elementos;
            for(var i = 1, cantidad = Number(cantidad); i <= cantidad; i++){
            var salto = document.createElement("P");
            var input = document.createElement("input");
            var text = document.createTextNode("Servicio " + i + ": ");
            input.setAttribute("name", "tipo" + i);
            input.setAttribute("size", "12");
            input.setAttribute("type","text");
            input.className = "input";
            salto.appendChild(text);
            salto.appendChild(input);
            div.appendChild(salto);
            }
        }
    function crearCostos(cantidad){
        var div = document.getElementById("campos_dinamicos2");
        while(div.firstChild)div.removeChild(div.firstChild); // remover elementos;
            for(var i = 1, cantidad = Number(cantidad); i <= cantidad; i++){
            var salto = document.createElement("P");
            var input = document.createElement("input");
            var text = document.createTextNode("Costo Servicio " + i + ": ");
            input.setAttribute("name", "costo" + i);
            input.setAttribute("size", "12");
            input.className = "input";
            input.setAttribute("type","text");
            salto.appendChild(text);
            salto.appendChild(input);
            div.appendChild(salto);
            }
        }
</SCRIPT> 

</head>
<body>
	<div class="grupo total">
			<div class="caja movil-100 cabecera">

				<header>
					<div class="menu_bar">
						<a href="#" class="bt-menu"><span class="icon-th-menu">menu</span>Menu</a>
					</div>
 
					<nav id = "nav1">
						<ul>
							<li><a href="inicio"><span class="icon-home3"></span>Inicio</a></li>
							<li><a href="#">Cotizaciones</a></li>
							<li class="submenu">
								<a href="#">Viajes  a Medida<span class="caret"></span></a>
								<ul class="children">
									<li><a href="#">Encontrar Viaje <span class="icon-zoom"></span></a></li>
									<li><a href="#">Crear Nuevo <span class="icon-edit"></span></a></li>
									
								</ul>
							</li>
							
							<li><a href="#">Paquetes</a></li>
							<li><a href="#">Nosotros</a></li>
							
						
						</ul>
					
					</nav>
				</header>
				
			</div>
		</div>
		
		<% if (misesion.getValue("username") != null){%>
		<br><br><br>
		<div class="grupo tabla">
			<div class="caja movil-100" id="repuestaCliente">
				
			</div>
			<div class="caja movil-25 cajaA">
			<center>
				<%if(tipo!= null && !tipo.equals("superusuario")) {%>
				<button class="button bt" id = "btn_modificar_adminUsuario"><center>Modificar datos</center></button>
				<%} %>
				<button class="btn_admin" id = "btn_gestionar_paquete"><center>Gestionar Paquetes</center></button>
				<button  class="btn_admin" id = "btn_gestionar_servicios"><center>Gestionar Servicios</center></button>
				<button  class="btn_admin" id = "btn_gestionar_clientes"><center>Gestionar Clientes</center></button>
				<button  class="btn_admin" id = "btn_gestionar_comentarios"><center>Gestor de Comentarios</center></button>
				<button  class="btn_admin" id = "btn_gestionar_proveedor"><center>Gestionar Proveedores</center></button>
				<%if(tipo!= null && tipo.equals("superusuario")) {%>
				<button  class="btn_admin" id = "btn_gestionar_administrador"><center>Gestionar Administradores</center></button>
				<%} %>
				
			</center>
			
			
			</div>
			<div class="caja movil-60 cajaB">
			
					<div id="respuesta_admin">
					dsfdsf
					</div>
				
				
					
					<div id="creardiv"   class="caja_centro">
					
						  <form action="crearAdministrador" method= "post" id="form_crearAdmin">
							
							<div id = "div_form">
								<label for ="nombres">Nombres:</label>
								<input type="text" name = "nombres" ><br>
							</div>
							<div id = "div_form">
								<label for ="apellidos">Apellidos:</label>
								<input type="text" name = "apellidos"><br>
							</div>
							<div id = "div_form">
								<label for ="dni">Dni:</label>
								<input type="text" name = "dni"><br>
							</div>
							<div id = "div_form">
								<label for ="telefono">Telefono:</label>
								<input type="text" name = "telefono" ><br>
							</div>
							<div id = "div_form">
								<label for ="correo">Correo:</label>
								<input type="text" name = "correo"><br>
							</div>
							<div id = "div_form">
								<label for ="username">Username:</label>
								<input type="text" name = "username" value ="admin" readonly="readonly"><br>
							</div>
							<div id = "div_form">
								<label for ="password">Password:</label>
								<input type="text" name = "password""><br><br>
							</div>
							<br><br>
							<center><input type="submit" value= "Crear" id = "btn_submit_crearAdmin" class = "button"></center>
						</form>
						
					</div>
				
			
			 		
			 		<div  id="crear_servicios" class="caja_centro">	
			 			<form action="crearRubro"  method = "get" id="form_crear_rubro">
			 				
			 				<div id="div_form">
			 					<label for = "rubro">Rubro:</label>
			 					<input type = "text" name = "rubro"><br>
			 				</div>
			 		
			 				<input type = "submit" class = "button" value = "A�adir Rubro">
			 			</form>	
			 		</div>
			 	
					<div id="crear_proveedores"  class = "caja_centro">
						<form action="crearProveedor" id="form_crear_proveedor" method = "post">
						
							<div id="div_form">
			 					<label for = "nombre">Nombre:</label>
			 					<input type = "text" name = "nombre"><br>
			 				</div>
			 				
			 				<div id="div_form">
			 					<label for = "ruc">RUC:</label>
			 					<input type = "text" name = "ruc"><br>
			 				</div>
			 				
			 				<div id="div_form">
			 					<label for = "correo">Correo:</label>
			 					<input type = "text" name = "correo"><br>
			 				</div>
			 				
			 				<div id="div_form">
			 					<label for = "telefono">Telefono:</label>
			 					<input type = "text" name = "telefono"><br>
			 				</div>
			 				
			 				<div id="div_form">
			 					<label for = "direccion">Direccion:</label>
			 					<input type = "text" name = "direccion"><br>
			 				</div>
			 				
			 				<div id="div_form">
			 				
			 					<label for = "ciudad">Ciudad:</label>
			 					
			 					<select  name = "ciudad" id = "selectC">
			 						<option value = "">Elija una Ciudad</option>
			 						<option value = "Arequipa">Arequipa</option>
			 						<option value = "Lima">Lima</option>
			 						<option value = "Cusco">Cusco</option>
			 						<option value = "Piura">Piura</option>
			 						<option value = "Loreto">Loreto</option>
			 						<option value = "Tacna">Tacna</option>
			 						<option value = "Ica">Ica</option>
			 					</select>
			 					<br>
			 					
			 				</div>
			 				
			 			  		<div id="div_form">
			 			  		
				 			    	<label for = "rubro">Rubro:</label>
				 			    	<select id = "select_rubros" name = "rubro" >
				 			    		<option>Elija un Rubro</option>		
				 			    	</select>
				 			    	<br>
				 			    	
			 			    	</div>
			 			    <div id="div_form">
			 			    	<label for = "cantidad" >Tipos de Servicio (Nro):</label>
			 			    	<input type="text"  name="cantidad" id="cantidad" value="" onkeyup="crearCampos(this.value);" > <br><br><br><br>
			 			    	<div id="campos_dinamicos"></div>
			 			    	
			 			    	<center><input type="button" id="botonCrearCosto" value="Agrega Precios" onclick="crearCostos(this.form.cantidad.value);" /><br></center>
			 			    	<div id="campos_dinamicos2"></div>
			 			    </div>
			 			    	
			 			    
			 			
			 			    <center><input type="submit" value = "Registrar" class="button bt"></center><br>
			 				<br>
						</form>
					
					</div>
			 
					<div id="modificar_proveedor"  class="caja_centro">
					
							<div id="div_form">
			 				
			 					<label for = "ciudad">Ciudad:</label>
			 					
			 					<select  name = "ciudad" id = "selectModProv">
			 						<option value = "">Elija una Ciudad</option>
			 						<option value = "Arequipa">Arequipa</option>
			 						<option value = "Lima">Lima</option>
			 						<option value = "Cusco">Cusco</option>
			 						<option value = "Piura">Piura</option>
			 						<option value = "Loreto">Loreto</option>
			 						<option value = "Tacna">Tacna</option>
			 						<option value = "Ica">Ica</option>
			 					</select>
			 					<br>
			 					
			 				</div>
			 				
			 			  	
				 			    	
				 			    	<form action="seleccionarProv" method = "post" id ="modificarEsteProv">
				 			    		<div id="div_form">
						 			    	<label for = "proveedor">Proveedor:</label>
						 			    	<select id = "select_proveedores" name = "proveedorid" >
						 			    		<option>Elija un Proveedor</option>		
						 			    	</select>
				 			    		</div>	
				 			    		
				 			    		<center><input type="submit" value = "Ir"></center>
				 			    	</form>
				 			    	<br>
				 			    	
			 			   <div id="buscando_prov">
			 			   </div>
			 			    
					
					</div>
			
					<div id="crearPaquete" class="caja_centro" >
					<form action="crearPaquete" method= "post" id="form_crearPaquete">
							
							<div id = "div_form">
								<label for ="nombre">Nombre:</label>
								<input type="text" name = "nombre" ><br>
							</div>
							<div id = "div_form">
								<label for ="descripcion">Descripcion:</label>
								<input type="text" name = "descripcion"><br>
							</div>
							<div id = "div_form">
								<label for ="costo">Costo:</label>
								<input type="text" name = "costo"><br>
							</div>
							<div id = "div_form">
								<label for ="origen">Origen:</label>
								<input type="text" name = "origen" ><br>
							</div>
							<div id = "div_form">
								<label for ="destino">Destino:</label>
								<input type="text" name = "destino"><br>
							</div>
							<div id = "div_form">
								<label for ="fecha">Fecha:</label>
								<input type="text" name = "fecha" value = "<%=currentDate%>" readonly="readonly"><br>
							</div>
							<div id = "div_form">
								<label for ="xreador">Creador:</label>
								<input type="text" name = "creador" value = "<%=misesion.getValue("username")%>" readonly="readonly"><br><br>
							</div>
							<br><br>
							<center><input type="submit" value= "Crear Paquete" id = "btn_submit_crearPaquete" class = "button"></center>
						</form>
						
				
					</div>
			
			</div>
			
			<div class="caja movil-15 cajaC">
				<div id="barra_paquete">
					<br><button class="tool" id="btn_crear_paquete"><center>Crear</center></button><br>
					<button  href="" class="tool" id="btn_modificar_paquete"><center>Modificar</center></button><br>
					<button  href="" class="tool" id="btn_eliminar_paquete"><center>Eliminar</center></button><br>
				</div>
				<div id="barra_admin">
					<br><button class="tool" id="btn_crear_admin"><center>CrearA</center></button><br>
					<button  class="tool" id="btn_modificar_admin"><center>ModificarA</center></button><br>
					<button  class="tool" id="btn_eliminar_admin"><center>EliminarA</center></button><br>
				</div>
				<div id="barra_servicios">
					<br><button class="tool" id="btn_crear_servicios"><center>CrearS</center></button><br>
					<button  class="tool" id="btn_modificar_servicios"><center>ModificarS</center></button><br>
					<button  class="tool" id="btn_eliminar_servicios"><center>EliminarS</center></button><br>
				</div>
				
				<div id="barra_proveedores">
					<br><button class="tool" id="btn_crear_proveedores"><center>CrearP</center></button><br>
					<button  class="tool" id="btn_modificar_proveedores"><center>ModificarP</center></button><br>
					<button  class="tool" id="btn_eliminar_proveedores"><center>EliminarP</center></button><br>
				</div>
				
			</div>
			
		
		</div>
		<%} %>
		
		
		
		
		<script src="js/jquery-1.12.4.min.js"></script>
		
		<script src="js/main.js"></script>

</body>
</html>