<%@page import="controllers.GestionPaquete"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8"%>
  <%@ page import = "java.util.*" %>  
<%@ page import = "model.*" %>
<%@ page import = "javax.servlet.http.HttpSession" %>
<%@ page import="java.util.List"%>


<% HttpSession misesion= request.getSession(); %>
<%
List<PaqueteCabecera> cabeceras = (List<PaqueteCabecera>)getServletContext().getAttribute("cabeceras");
Cliente cliente = (Cliente)getServletContext().getAttribute("cliente");
Boolean exito = (Boolean)getServletContext().getAttribute("exitoDetalle");
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Inicio</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<link href="fonts/style.css" rel="stylesheet" >

	<link rel="stylesheet" href="css/estilo.css">
	<link rel="stylesheet" href="css/ed-grid.css">
	
	<script src="js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
	
	
</head>

	<body class = "">
		
		<div class="grupo total">
			<div class="caja movil-100 cabecera">

				<header>
					<div class="menu_bar">
						<a href="#" class="bt-menu"><span class="icon-th-menu">menu</span>Menu</a>
					</div>
 
					<nav id = "nav1">
						<ul>
							<li><a href="inicio"><span class="iconM-home"></span>Inicio</a></li>
							<li><a href="#"><span class="iconM-text-document"></span>Cotizaciones</a></li>
							<li class="submenu">
								<a href="#"><span class="iconM-aircraft"></span>Viajes  a Medida<span class="caret"></span></a>
								<ul class="children">
									<li><a href="#">Encontrar Viaje <span class="icon-zoom"></span></a></li>
									<li><a href="#">Crear Nuevo <span class="icon-edit"></span></a></li>
									
								</ul>
							</li>
							
							<li><a href="#"><span class="iconM-suitcase"></span>Paquetes</a></li>
							<li><a href="#">Nosotros</a></li>
							
						
						</ul>
					</nav>
				</header>
				
			</div>
			
			<div class="grupo total" id = "header_titulo">
				<div class="caja movil-100 logo">
					<h1><a href="#" id="titulo">Viajes SinLimites</a></h1>
				</div>
			</div>
	
			<div class="grupo total  modal" id="modal">
				<div class="caja movil-100 header_modal">
					<h2>Registro de Cliente</h2>
				</div>
				<div class = "caja movil-100 ">
					<form action = "registro" method = "post" id ="form_registro"  >
					
					
							<fieldset class="fieldset2 caja_form">
								<legend>Personal</legend>
								<label for="nombres"class="label_modal">Nombres:</label>
								<input type ="text" name= "nombres"  class="textbox_modal" /><br><br>
						
								<label for="apellidoP"class="label_modal">Apellido Paterno:</label>
								<input type ="text" name= "apellidoP"  class="textbox_modal"/><br><br>
						
								<label for="apellidoM"class="label_modal">Apellido Materno:</label>
								<input type ="text" name= "apellidoM"  class="textbox_modal"/><br><br>
						
								<label for="dni"class="label_modal">Dni:</label>
								<input type ="text" name= "dni"  class="textbox_modal" /><br><br>
						
							</fieldset>
						
							<fieldset class="fieldset2">
								<legend>Contacto</legend>
								<label for="direccion"class="label_modal">Direccion:</label>
								<input type ="text" name= "direccion"  class="textbox_modal"/><br><br>
						
								<label for="fechaNacimiento"class="label_modal">Fecha de nacimiento:</label>
								<input type ="text" name= "fechaNacimiento"  class="textbox_modal" /><br><br>
						
								<label for="telefono"class="label_modal">Telefono:</label>
								<input type ="text" name= "telefono"  class="textbox_modal" /><br><br>
							</fieldset>
							<fieldset class="fieldset2">
								<legend> Datos Usuario</legend>
								<label for="correo"class="label_modal">Correo:</label>
								<input type ="text" name= "correo"  class="textbox_modal" /><br><br>
						
								<label for="username"class="label_modal">Username:</label>
								<input type ="text" name= "username"  class="textbox_modal" /><br><br>
						
								<label for="password"class="label_modal">Contraseña:</label>
								<input type ="text" name= "password"  class="textbox_modal" /><br><br>
								
								<input type="radio" name="sexo" value="hombre" required> Hombre<br>
  								<input type="radio" name="sexo" value="mujer"required> Mujer<br>
  								
							
								
							</fieldset>
							
					
						<div class="btn_form">
						<br><br>
							<input type="submit"  class= "button" value="Registrar" id = "btn_registro" />
						</div>
						<div id="respuesta">
						</div>
						
						</form>
					</div>
				
				
				
				
			</div>
			
		
			
			
			<div class="grupo total" id = "baner">
				<div class="caja tablet-75">
					<p>.</p>
				</div>
				
				<div class ="caja tablet-25" id="baner_caja">
					<div id = "respuesta_login">
					<%if (exito != null && exito) {%>
						<%
						
							exito = false; 
							getServletContext().setAttribute("exitoDetalle",exito);
						%>
						<h5>Se Reservo el paquete</h5>
					<%} %>
					</div>
					<% if (misesion.getValue("username") == null){%>
					
					<form action = "login" method = "post" id = "form_login">
						<label for="username"class="label_modal">Usuario/Correo Electronico:</label><br>
						<input type ="text" name= "username"  class="textbox" placeholder="Nombre de Usuario"/><br><br>

						<label for="password"class="label_modal">Contraseña:</label><br>
						<input type ="password" name="password" class="textbox" placeholder="contraseña"/><br><br>

						<input type="submit"  class= "btn_sesion"  value="Iniciar" /><br><br>
					<div class="enlace">
						
						<a href="#" id = "log">Registrarse...</a>
						
					</div>
						
					</form>
					<% }else{%>
					
					<% if (misesion.getValue("sexo").equals("hombre")) {%>
					
						<img src="img/usuarioM.jpg" alt="usuarioM" class = "tablet-60 circulo">
					<%}%>
					<% if (misesion.getValue("sexo").equals("mujer")) {%>
					
						<img src="img/usuarioF.jpg" alt="usuarioF" class = "tablet-60 circulo">
					<%}%>
					
					<br>
					<h1 class = "font_perfil">${sessionScope.username}</h1>					
					<br>
					<%if (misesion.getValue("username").equals("admin")) { %>
						<p><a href= "inicio_admin.jsp" class ="btn_sesion" >Ir a Perfil</a></p>
					<%}else{ %>
						<p><a href= "inicio_cliente.jsp" class ="btn_sesion" >Ir a Perfil</a></p>
					<% }%>
					<br><br><a href= "sessionControl" class ="a_perfil" id = "a_perfil">Cerrar Sesion</a>
					<% }%>
				</div>
			</div>
	
		</div>
		
		<!--  -->
		<br><br>
		<div class="grupo paqueteA">
			<div class="caja movil-100" >
				<h2 class = "paquete_nombre"><span class=" iconM-images"></span> Paquetes Internacionales</h2><br>
			</div>
			<div class="caja movil-1-3 centrar-contenido" id ="ol1">
				
				<img src="img/pi1.jpg" alt="Imagen 1"  border="3" class = "img_rad">
				<br>
				<center>
				<h3 class = "titulo">India via Star Global</h3> 
				<h4 class = "descripcion"> 7Días – 6Noches</h4>
				<h4 class = "descripcion">Desde $ 2100.00 | S/.6600.5</h4><br>
				<button class = "btn_paq">Ver Info </button>
				</center>
			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pi2.jpg" alt="Imagen 2"  border="3" class = "img_rad" >
				<br>
				<center>
				<h3 class = "titulo">Teotihuacán via Star México</h3> 
				<h4 class = "descripcion"> 5Días – 4Noches</h4>
				<h4 class = "descripcion">Desde $ 700.00 | S/.2400.0</h4><br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pi3.jpg" alt="Imagen 3"  border="3" class = "img_rad">
				<br>
				<center>
				<h3 class = "titulo">Sidney via Star Global</h3> 
				<h4 class = "descripcion"> Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 3500.00 | S/.11050.0</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			
		</div>
	<br>
		<div class="grupo paqueteB">
			<div class="caja movi-100">
			<br><br>
				<strong><h2 class = "paquete_nombre"><span class=" iconM-images"> Paquetes Nacionales</h2><br></strong>
			</div>
			<div class="caja movil-1-3">
				
				<img src="img/pn1.jpg" alt=" Imagen 4"  border="3">
				<br>
				<center>
				<h3 class = "titulo">Amazonas via Star Perú</h3> 
				<h4 class = "descripcion"> 4Días – 3 Noches</h4>
				<h4 class = "descripcion">Desde $ 230.00 | S/.750.0</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pn2.jpg" alt="Imagen 5" border="3">
				<br>
				<center>
				<h3 class = "titulo">Huancayo via Star Perú</h3> 
				<h4 class = "descripcion"> 3Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 160.00 | S/.500.5</h4>
				<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			<div class="caja movil-1-3">
							
				<img src="img/pn3.jpg" alt="Imagen 6" border="3">
				<br>
				<center>
				<h3 class = "titulo">Arequipa via Star Perú</h3> 
				<h4 class = "descripcion"> 3Días – 2 Noches</h4>
				<h4 class = "descripcion">Desde $ 197.00 | S/.689.5</h4>
			<br>
				<button class = "btn_paq">Ver Info </button>
				</center>

			</div>
			
		</div>
		<br>
		<div class="grupo paqueteC">
			<div class="caja movil-100" >
				<h2  class = "paquete_nombre"><span class=" iconM-images"> Paquetes Destacados</h2><br>
			</div>
		
		<% if (cabeceras != null && cabeceras.size()>0){%>
		<% if (cabeceras.size()>=3){for(int i=cabeceras.size()-1; i>=cabeceras.size()-3;i-- ) { %>
			<div class="caja movil-1-3 centrar-contenido" id ="ol1">
				
				<img src="img/pi1.jpg" alt="Imagen 1"  border="3">
				<br>
				<center>
				<h3 class = "titulo"><%=cabeceras.get(i).getNombre() %></h3> 
				<h4 class = "descripcion"><%=cabeceras.get(i).getDescripcion() %></h4>
				<h4 class = "descripcion">Desde<%=cabeceras.get(i).getCosto()%></h4>
				<% if(misesion.getValue("username") == null || misesion.getValue("username").equals("admin")){%>
				
					<a href="#baner_caja" id= "btn_debe_registrarse" >Reservar</a>
			
				<%} else{%>
						<form action="registroDetalle"   id="form_registro_detalle" method= "post">
						
							<input type="hidden" name = "id_cabecera" value= "<%=cabeceras.get(i).getId()%>"><br>
							<input type="hidden" name = "id_cliente" value= "<%=cliente.getId()%>"><br>
							<input type = "submit" id ="btn_submit_informar" value = "Reservar">
						</form>
					
				<%} %>
				
				</center>
			</div>
		<%} }else{for(int i=0; i<cabeceras.size();i++ ) {%>
			
			<div class="caja movil-1-3 centrar-contenido" id ="ol1">
				
				<img src="img/pi1.jpg" alt="Imagen 1"  border="3">
				<br>
				<center>
				<h3 class = "titulo"><%=cabeceras.get(i).getNombre() %></h3> 
				<h4 class = "descripcion"><%=cabeceras.get(i).getDescripcion() %></h4>
				<h4 class = "descripcion">Desde<%=cabeceras.get(i).getCosto()%></h4>
				
				<% if(misesion.getValue("username") == null ||misesion.getValue("username").equals("admin")){%>
				
					<a href="#baner_caja" id= "btn_debe_registrarse" >Reservar</a>
			
				<%} else{%>
						<form action="registroDetalle"   id="form_registro_detalle" method= "post">
						
							<input type="hidden" name = "id_cabecera" value= "<%=cabeceras.get(i).getId()%>"><br>
							<input type="hidden" name = "id_cliente" value= "<%=cliente.getId()%>"><br>
							<input type = "submit" id ="btn_submit_informar" value = "Reservar">
						</form>
					
				<%} %>
				
				</center>
			</div>
		<%} }%>
		<%} %>
		<div class="caja movil-100" id = "respuesta_reg">
			
		</div>
		</div>
		
		<br><br><br>
		<div class="grupo total">
				
			<footer>

				<div  class="caja tablet-total desde-tablet ">
					<img src="img/imgfooter.jpg" alt="Slider Imagen 1" class ="centro">
				</div>

				<div class="grupo tabla">
					<div class=" caja tablet-1-3 desde-tablet caja_f">
						<h3>Servicios</h3>
							<nav >
								<ul>
									<li><a href="#"><span class="icon-upload-to-cloud"></span>Paquetes Nacionales</a></li>
									<li><a href="#"><span class="icon-laptop"></span>Paquetes Internacionales</a></li>
									<li><a href="#"><span class="icon-tablet"></span>Reservaciones</a></li>
									<li><a href="#"><span class="icon-tv"></span>Viajes a Medida</a></li>
									<li><a href="#"><span class="icon-tv"></span>Movilidad</a></li>
								</ul>
							</nav>
					</div>

					<div class="caja tablet-1-3 desde-tablet caja_f">
						<h3>Nosotros</h3>
						<nav >
							<ul>
								<li><a href="#">Acerca de Viaje SinLimites</a></li>
								<li><a href="#">Visión</a></li>
						
								<li><a href="#">Consultas y Sugerencias</a></li>
							</ul>
						</nav>
					</div>

					<div class="caja movil-100 tablet-1-3 caja_f">
						<h3>Contacto</h3>
						<nav>
							<ul>
								<li><a href="#">Teléfono: (054)904523</a></li>
								<li><a href="#">E: informes@viajesinlimites.com</a></li>
						
								<li><a href="#">www.viajesinlimites.appspot.com</a></li>
							</ul>
						</nav>
					</div>
				</div>
				
				<div class="caja movil-100 cop">
					
					<center><h5>&copy; 2016 Viaje SinLimites</h5></center>
				</div>
			</footer>
		</div>
	<script src="js/jquery-1.12.4.min.js"></script>
	
	<script src="js/main.js"></script>
	
	</body>

</html>
