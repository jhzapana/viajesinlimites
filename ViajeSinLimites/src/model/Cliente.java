package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Cliente{
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombres;
	
	@Persistent
	private String apellidoP;
	
	@Persistent
	private String apellidoM;
	
	@Persistent
	private String dni;
	
	@Persistent
	private String direccion;
	
	@Persistent
	private String fechaNac;
	
	@Persistent
	private String telefono;
	
	@Persistent
	private String correo;
	
	@Persistent
	private String username;
	
	@Persistent
	private String password;
	
	@Persistent
	private String sexo;
	
	@Persistent
	private String estReg;
	
	
	public Cliente(String nombre,String apellidoP,String apellidoM,String dni,String direccion,String fechaNac,String telefono,String correo,String usuario,String password,String sexo){
		super();
		this.nombres			= nombre;
		this.apellidoP		= apellidoP;
		this.apellidoM		= apellidoM;
		this.dni			= dni;
		this.direccion		= direccion;
		this.fechaNac		= fechaNac;
		this.telefono		= telefono;
		this.correo			= correo;
		this.username        = usuario;
		this.password       = password;
		this.sexo			= sexo;
		this.estReg			= "A";
	}
	public Cliente(){
		super();
	}
	public String getId() {
		return KeyFactory.keyToString(id);
	}

	public void setId(String idClient) {
		Key keyCliente = KeyFactory.stringToKey(idClient);
		this.id = KeyFactory.createKey(keyCliente,
		PaqueteDetalle.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}

	

	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo){
		this.sexo = sexo;
		
	}
	
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombre){
		this.nombres = nombre;
		
	}
	
	public String getApellidoP() {
		return apellidoP;
	}
	public void setApellidoP(String apellidoP){
		this.apellidoP = apellidoP;
	}
	
	public String getApellidoM() {
		return apellidoM;
	}
	public void setApellidoM(String apellidoM){
		this.apellidoM = apellidoM;
	}
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni){
		this.dni = dni;
	}
	
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion){
		this.direccion = direccion;
	}
	
	
	public String getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(String fechaNac){
		this.fechaNac = fechaNac;
	}
	
	
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo){
		this.correo = correo;
	}
	
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg){
		this.estReg = estReg;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String usuario) {
		this.username = usuario;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
