package model;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Administrador{
	
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key id;
	
	@Persistent
	private String nombres;
	
	@Persistent
	private String apellidos;
	
	
	@Persistent
	private String dni;
	
	
	@Persistent
	private String telefono;
	
	@Persistent
	private String correo;
	
	@Persistent
	private String username;
	
	@Persistent
	private String password;
	
	
	@Persistent
	private String estReg;
	
	
	public Administrador(String nombre,String apellidos,String dni,String telefono,String correo,String password){
		super();
		this.nombres			= nombre;
		this.apellidos		= apellidos;
	
		this.dni			= dni;
	
		this.telefono		= telefono;
		this.correo			= correo;
		this.username        = "admin";
		this.password       = password;
	
		this.estReg			= "A";
	}
	public Administrador(){
		
	}
	public String getIdCurso() {
		return KeyFactory.keyToString(id);
	}

	public void setIdCurso(String idadmin) {
		Key keyad = KeyFactory.stringToKey(idadmin);
		this.id = KeyFactory.createKey(keyad,
		PaqueteCabecera.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}
	
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombre){
		this.nombres = nombre;
		
	}
	
	
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos){
		this.apellidos = apellidos;
	}
	
	public String getDni() {
		return dni;
	}
	public void setDni(String dni){
		this.dni = dni;
	}
	
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo){
		this.correo = correo;
	}
	
	public String getEstReg() {
		return estReg;
	}
	public void setEstReg(String estReg){
		this.estReg = estReg;
	}
	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
}
