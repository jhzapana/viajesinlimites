package controllers;


import java.io.IOException;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;






import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;
import model.PaqueteDetalle;
import model.Proveedor;
import model.Rubro;

@SuppressWarnings("serial")
public class SeleccionarProveedor extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean status = false;
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Key keyp = KeyFactory.stringToKey(req.getParameter("proveedorid"));
		Proveedor p = pm.getObjectById(Proveedor.class, keyp);
		
		getServletContext().setAttribute("miProveedor",p);
		
		status = true;
		
		map.put("exito", status);
		write(resp,map);
		System.out.println("asdasd");
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
