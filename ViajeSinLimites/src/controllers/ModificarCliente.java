package controllers;


import java.io.IOException;

import javax.jdo.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import model.PMF;
import model.Cliente;

@SuppressWarnings("serial")
public class ModificarCliente extends HttpServlet  {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		
		String id = req.getParameter("id");
		
		String nombres = req.getParameter("nombres");
		String apellidoP = req.getParameter("apellidoP");
		String apellidoM = req.getParameter("apellidoM");
		

		String telefono = req.getParameter("telefono");
		String correo = req.getParameter("correo");
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		//Key key = KeyFactory.stringToKey(req.getParameter("id"));

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		//Cliente found = pm.getObjectById(Cliente.class, key);
		
		Query q = pm.newQuery(Cliente.class);
		q.setFilter("username == usernameParam");
		q.declareParameters("String usernameParam");
		
		try{
			List<Cliente> alumno = (List<Cliente>) q.execute(username);
			for(Cliente p: alumno){
				p.setNombres(nombres);
				p.setApellidoP(apellidoP);
				p.setApellidoM(apellidoM);			
				p.setTelefono(telefono);		
				p.setCorreo(correo);				
				p.setUsername(username);		
				p.setPassword(password);
				
				
				getServletContext().setAttribute("cliente",p);
				
				
				
			}	
			exito = true;
			map.put("username", username);
			map.put("isValid", exito);
		
			
		}catch(Exception e){
	
		}finally{
			q.closeAll();
		}
		

		write(resp,map);
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
