package controllers;


import java.io.IOException;


import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Transaction;











import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.gson.Gson;

import model.PMF;
import model.Cliente;
import model.PaqueteCabecera;
import model.PaqueteDetalle;

@SuppressWarnings("serial")
public class RegistroDetalle extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		Map<String, Object> map = new HashMap<String,Object>();
		boolean exito = false;
		
		
	
		
		
		Key keycabecera = KeyFactory.stringToKey(req.getParameter("id_cabecera"));
		Key keycliente = KeyFactory.stringToKey(req.getParameter("id_cliente"));
		
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		
		Transaction tx = pm.currentTransaction();
		tx.begin();
		
		try{
			PaqueteCabecera cabecera = pm.getObjectById(PaqueteCabecera.class, keycabecera);
			Cliente cliente = pm.getObjectById(Cliente.class, keycliente);
			//Color copy = new Color(found.getName());
			
			
			
			PaqueteDetalle p = new PaqueteDetalle();
			p.setCabecera(cabecera);
			p.setCliente(cliente);
			//p.setColor(copy);
			try{
				pm.makePersistent(p);
				tx.commit();
				
				exito = true;
				getServletContext().setAttribute("exitoDetalle",exito);
				resp.sendRedirect("inicio");
				
			}catch(Exception e){
				System.out.println(e);
			
				
			}
		}catch(Exception e){
			System.out.println(e);
		}finally{
			try {
				if (tx.isActive())
					tx.rollback();
            } finally {
            	pm.close();
            }
		}
	
	}



}
