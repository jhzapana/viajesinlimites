package controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.PersistenceManager;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.jdo.Query;

import com.google.gson.Gson;

import model.Cliente;
import model.PMF;

@SuppressWarnings("serial")
public class Login  extends HttpServlet{
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		Map<String, Object> map = new HashMap<String,Object>();
		boolean error = true;
		boolean admin= false;
		boolean client = false;
		
		String username = req.getParameter("username");
		String password = req.getParameter("password");
		
		
		if(username.equals("admin")){
			if(password.equals("admin")){
				getServletContext().setAttribute("tipo","superusuario");
				
				HttpSession misesion = req.getSession(true);
				
				misesion.setAttribute("username",username);
				misesion.setAttribute("id",username);
				misesion.setAttribute("sexo","hombre");
				admin = true;
				error = false;
			}
			
			
			
		}
		else{
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Cliente.class,
				"username =='"+username+"' "+
				"&& password =='"+password+"' "		
				);
		Cliente info = new Cliente();
		
		
			try{
				List<Cliente> nuevo = (List<Cliente>) q.execute();
				for(Cliente p: nuevo){
					info = p;
					if(p.getNombres()!=null){
						error= false;
						client = true;
					}
				}
				HttpSession misesion = req.getSession(true);
				
				misesion.setAttribute("username",info.getUsername());
				
				misesion.setAttribute("sexo",info.getSexo());
				getServletContext().setAttribute("cliente",info);
				
				
				
			}catch(Exception e){
		
				}finally{
					q.closeAll();
				}
		
		}
		map.put("admin", admin);
		map.put("client", client);
		map.put("error", error);
		write(resp,map);
		
		
	
	}
	public void write(HttpServletResponse resp,Map <String,Object> map) throws IOException{
		resp.setContentType("application/json");
		resp.setCharacterEncoding("UTF-8");
		resp.getWriter().write(new Gson().toJson(map));
	}

}
